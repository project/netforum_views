var keepHelpCollapsed = false;

if (Drupal.jsEnabled){
  $(document).ready(function(){
    $('#edit-obj-type-name').bind("blur",setObjectType);
  });
}


function setObjectName(obj_type_name){
  obj_type_name += "";
  if (obj_type_name.toString().toLowerCase() != $('#edit-obj-type-name').val().toLowerCase() && obj_type_name != ''){
    $("#edit-obj-type-name").fadeOut("normal", function() { $(this).val(obj_type_name).blur(); }).fadeIn("normal");
  }
  return false;
}

function setObjectType(){
  //  If there's an autocomplete box up then wait till it closes to make our move.
  if ($('#autocomplete').length && $('#autocomplete').css("visibility") == 'visible'){
    return;
  }

  var objName = $(this).val();
  var prevObjName = '';
  if ( $('#edit-key-obj-type-name').length ) {
    prevObjName = $('#edit-key-obj-type-name').val();
  }
  if ( objName.toLowerCase() != prevObjName.toLowerCase()) {
   
      //update our autocomplete
      var newAutocomplete = location.protocol + "//" + location.host + BASE_URL + 'admin/content/netforum-object-fields-autocomplete/' + escape(objName);
      $('#edit-obj-field-help-autocomplete').val(newAutocomplete);
      //the url should have changed from something like http://www.example.com/admin/content/netforum-object-fields-autocomplete/9afef9a3-69fc-4c61-8114-02baefca77e7
      // to http://www.example.com/admin/content/netforum-object-fields-autocomplete/Individual
      
      //Remove the previous autocompletes
      $('#edit-obj-field-help').unbind('keyup');
      $('#edit-obj-field-help').unbind('keydown');
      $('#edit-obj-field-help').unbind('blur');
      //re-bind the autocomplete actions to the form
      Drupal.autocompleteAutoAttach();
      
  }  
  
  return false;
}

